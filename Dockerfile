FROM wordpress:5.1.1-php7.3-apache

COPY wp-content /var/www/html/wp-content
COPY uploads.ini /usr/local/etc/php/conf.d/uploads.ini

# Reconfigure Apache2
COPY ports.conf /etc/apache2/
RUN unlink /etc/apache2/sites-enabled/000-default.conf

# Setup adminer installation 
COPY adminer.conf /etc/apache2/sites-available
RUN	mkdir /var/www/adminer && \
	curl -L -o /var/www/adminer/adminer.php https://github.com/vrana/adminer/releases/download/v4.3.1/adminer-4.3.1.php && \
	chown -R www-data:www-data /var/www/adminer && \
	ln -s /etc/apache2/sites-available/adminer.conf /etc/apache2/sites-enabled/adminer.conf